= TrioVis =

TrioVis is a visualisation tool developed to assist filtering on coverage and variant frequency for genomic variants from exome sequencing of parent-child trios. It organises the variant data by grouping each variant based on the laws of Mendelian inheritance. Taking three Variant Call Format (VCF) files as input, the tool provides a user interface to test different coverage thresholds (i.e. different levels of stringency), to find the optimal threshold values, and to gain insights into the global effects of filtering.

The TrioVis repository is available at the [BitBucket account of the Visual Data Analysis lab](http://bitbucket.org/vda-lab/triovis/)
